jest.mock('../src/service');

import { postPayment } from '../src/controller';
import { doPayment } from '../src/service';
import httpMocks from 'node-mocks-http';
import { mockFunction } from './util';

describe('Payment Controller', () => {
  describe('Post Payment', () => {
    test('When the body is empty, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {},
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the card is invalid, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: 'notcard',
            cvc: '1234',
            exp: '05/2100',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the cvc is invalid, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '12341111',
            exp: '05/2100',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the date is expired, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '1234',
            exp: '05/2019',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the date is invalid, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '1234',
            exp: '0a2100',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the date is to short, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '1234',
            exp: '02100',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the date is to long, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '1234',
            exp: '02/0/21',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the amount is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '1234',
            exp: '05/2100',
          },
          amount: -100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the data is valid and doPayment returns success, then the statusCode is 200', async () => {
      const mockedDoPayment = mockFunction(doPayment);
      mockedDoPayment.mockImplementation(async (card: string, amount: number) => {
        return { status: 200, message: 'Message' };
      });
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '1234',
            exp: '05/2100',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(200);
    });
    test('When the data is valid and doPayment fails, then the statusCode is 400', async () => {
      const mockedDoPayment = mockFunction(doPayment);
      mockedDoPayment.mockImplementation(async (card: string, amount: number) => {
        return { status: 400, message: 'Message' };
      });
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '1234',
            exp: '05/2100',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the data is valid and doPayment throws an error, then the statusCode is 500', async () => {
      const mockedDoPayment = mockFunction(doPayment);
      mockedDoPayment.mockImplementation((card: string, amount: number) => {
        throw new Error();
      });
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/payment',
        body: {
          card: {
            card: '4242424242424242',
            cvc: '1234',
            exp: '05/2100',
          },
          amount: 100,
        },
      });
      const response = httpMocks.createResponse();

      await postPayment(request, response);

      expect(response.statusCode).toEqual(500);
    });
  });
});
