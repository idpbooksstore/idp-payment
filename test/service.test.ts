jest.mock('../src/model');

import { doPayment } from '../src/service';
import { doTransaction } from '../src/model';
import { mockFunction } from './util';

describe('Payment Service', () => {
  describe('Do Payment', () => {
    it('When the card is not valid, then the status is 400', async () => {
      const mockedDoTransaction = mockFunction(doTransaction);
      mockedDoTransaction.mockImplementation(async (card: string, amount: number) => {
        if (card === '1234') return { status: 400, message: 'Message' };
        return { status: 200, message: 'Message' };
      });

      const result = await doPayment('1234', 100);

      expect(result.status).toEqual(400);
    });
    it('When there are not sufficient funds, then the status is 400', async () => {
      const mockedDoTransaction = mockFunction(doTransaction);
      mockedDoTransaction.mockImplementation(async (card: string, amount: number) => {
        if (card === '1234' && amount > 100) return { status: 400, message: 'Message' };
        return { status: 200, message: 'Message' };
      });

      const result = await doPayment('1234', 101);

      expect(result.status).toEqual(400);
    });
    it('When there are sufficient funds, then the status is 200', async () => {
      const mockedDoTransaction = mockFunction(doTransaction);
      mockedDoTransaction.mockImplementation(async (card: string, amount: number) => {
        if (card === '1234' && amount > 100) return { status: 400, message: 'Message' };
        return { status: 200, message: 'Message' };
      });

      const result = await doPayment('1234', 100);

      expect(result.status).toEqual(200);
    });
  });
});
