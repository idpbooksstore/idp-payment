import { doPayment } from './service';
import Router, { Request, Response } from 'express';
import { validate } from './validate';

interface PaymentData {
  card: {
    card: string;
    cvc: string;
    exp: string;
  };
  amount: number;
}

export const router = Router();

export const postPayment = async (req: Request, res: Response) => {
  if (!validate(req.body)) return res.status(400).send({ message: validate.errors });
  try {
    const {
      card: { card, cvc, exp },
      amount,
    } = req.body as PaymentData;
    const { status, message } = await doPayment(card, amount);

    res.status(status).send({ message });
  } catch (error) {
    return res.status(500).send({ message: error });
  }
};

router.post('/', postPayment);
