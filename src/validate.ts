import Ajv from 'ajv';
import { card, expiration, cvc } from 'creditcards';

const ajv = new Ajv();

ajv.addFormat('card', {
  type: 'string',
  validate: (x) => card.isValid(x),
});

ajv.addFormat('cvc', {
  type: 'string',
  validate: (x) => cvc.isValid(x),
});

ajv.addFormat('exp', (exp) => {
  const date = exp.split('/').map((x) => +x);
  if (date.length !== 2) return false;
  return isFinite(date[0]) && isFinite(date[1]) && !expiration.isPast(date[0], date[1]);
});

ajv.addFormat('amount', {
  type: 'number',
  validate: (x) => x >= 0 && isFinite(x),
});

const schema = {
  type: 'object',
  properties: {
    card: {
      type: 'object',
      properties: {
        card: { type: 'string', format: 'card' },
        cvc: { type: 'string', format: 'cvc' },
        exp: { type: 'string', format: 'exp' },
      },
      required: ['card', 'cvc', 'exp'],
      additionalProperties: false,
    },
    amount: { type: 'number', format: 'amount' },
  },
  required: ['card', 'amount'],
  additionalProperties: false,
};

export const validate = ajv.compile(schema);
