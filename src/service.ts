import { doTransaction } from './model';

export const doPayment = async (card: string, amount: number) => doTransaction(card, amount);
