const cards: { [key: string]: number } = {
  '4242424242424242': 100,
  '378282246310005': 500,
};

interface TransactionResult {
  status: number;
  message: string;
}

export const doTransaction = async (card: string, amount: number): Promise<TransactionResult> => {
  if (!(card in cards)) return { status: 400, message: 'Invalid Card' };
  if (cards[card] < amount) return { status: 400, message: 'Invalid Amount' };
  return { status: 200, message: 'Success' };
};
