# IDP Payment Service

This module is used to simulate a credit card transaction.

It uses the route `POST /api/payment` to receive credit card information and an amount to pay. The body of the request has to be a json string with the following format: `{card: {card: string, cvc: string, exp: string}, amount: int}`. The format for the `card number` and `cvc` is checked using the [creditcards](https://www.npmjs.com/package/creditcards) package. Additionally the `exp` string must have the format `MM/YYYY`. If the received data is valid then it replies with the transaction status.

The module responds with an object of type `{status: int, message: string}`. Codes for status can be found [here](./src/status.ts). In case of an error the message string can contain some useful information.

Response statusCode:
- `200` when the data is valid and there are no errors
- `400` if the data is invalid
- `500` if there are errors at runtime

## Routes

- `POST /api/payment`
  - Body: 
  ```typescript
  {card: {card: string, cvc: number, date: string}, amount: number}
  ```
  - Output: 
  ```typescript
  {
    status: StatusCode, 
    message: string
  }
  ```

## Docker Container

This module can be built as a docker container.

#### Environment Variables

- `PORT` - The port the server will listen on. Default to `8080`
- `HOST` - Default to `localhost`
- `NODE_ENV` - `production` or `development`

## Available Scripts

In the project directory, you can run:

### `npm run build`

First it runs the linter and the formatter.<br />
Builds the app for production to the `dist` folder.<br />

### `npm run start`

Runs the build from `dist`.<br />

### `npm run test`

Launches the test runner.<br />

### `npm run format`

Runs the formatter.<br />

### `npm run lint`

Runs the linter.<br />

### `npm run prebuild`

Runs the linter and the formatter.<br />

### `npm run build:live`

Runs the code in watch mode using nodemon and ts-node.<br />
